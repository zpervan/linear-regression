#include <iostream>
#include <vector>
#include <math.h>
#include "../include/LinearRegression.h"

struct coefficients
{
    double averageX;
    double averageY;
    double B1;
    double B0;
};

int main() {
    std::vector<double> valueX = { 10, 6, 8, 9, 10, 5 };
    std::vector<double> valueY = { 2, 4, 5, 4, 5, 3};

    LinearRegression* LN = new LinearRegression();
    coefficients* coef = new coefficients;

    coef->averageX = LN->calculateAverageX(valueX);
    coef->averageY = LN->calculateAverageY(valueY);
    coef->B1 = LN->calculateCoefB1(valueX, valueY, coef->averageX, coef->averageY);
    coef->B0 = LN->calculateCoefB0(coef->averageX, coef->averageY, coef->B1);

    std::cout << "Average X value: " << coef->averageX << std::endl;
    std::cout << "Average Y value: " << coef->averageY << std::endl;
    std::cout << "Coefficent B1 value: " << coef->B1 << std::endl;
    std::cout << "Coefficent B0 value: " << coef->B0 << std::endl;

    std::cout << "Linear regression formula: " << std::endl;
    std::cout << "Y = " << LN->roundTwoDecimals(&coef->B0) / 100 << " + " << LN->roundTwoDecimals(&coef->B1)  << " * x" << std::endl;

    delete LN, coef;
    return 0;
}